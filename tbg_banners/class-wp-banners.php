<?php



class wpbanners extends WP_Widget {
    // class constructor
    public function __construct() {
        $widget_ops = array(

        );
        parent::__construct( 'wpbanners', 'Banners', $widget_ops );
    }

    // output the widget content on the front-end
    public function widget( $args, $instance ) {

        global $TBGBannerSet;

        if(!isset($TBGBannerSet)){

            $TBGBannerSet = true;
        }
        // PART 1: Extracting the arguments + getting the values
        extract($args, EXTR_SKIP);
        $type = empty($instance['type']) ? '' : $instance['type'];
        $class = empty($instance['class']) ? '' : $instance['class'];


        if (!empty($type) and $type=="banner_header"){
            echo '<div class="addSup'.($class!=''?' '.$class:'').'"><script>Banners.set("super");</script></div>';
        }

        if (!empty($type) and $type=="banner_cubo"){

            echo '<div class="addBox'.($class!=''?' '.$class:'').'"><script>Banners.set("cubo");</script></div>';

        }

	if (!empty($type) and $type=="banner_half"){

            echo '<div class="addBox'.($class!=''?' '.$class:'').'"><script>Banners.set("half");</script></div>';

        }

    }


    // output the option form field in admin Widgets screen
    public function form( $instance ) {
        $type = $instance['type'];
        $class = $instance['class'];

        // PART 2-3: Display the fields
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('type'); ?>">Tipo de Banner:</label>
            <select class='widefat' id="<?php echo $this->get_field_id('type'); ?>"
                    name="<?php echo $this->get_field_name('type'); ?>" type="text">
                <option value='banner_header'<?php echo ($type=='banner_header')?'selected':''; ?>>
                    Leader Bord
                </option>
                <option value='banner_cubo'<?php echo ($type=='banner_cubo')?'selected':''; ?>>
                    Box Banner
                </option>
                <option value='banner_half'<?php echo ($type=='banner_half')?'selected':''; ?>>
                    Box Half
                </option>

            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('class'); ?>">Clase (Opcional):</label>
            <input class="widefat" type="text" id="<?php echo $this->get_field_id('class'); ?>" name="<?php echo $this->get_field_name('class'); ?>" value="<?=$class?>">
        </p>
        <!-- Widget City field END -->
        <?php
    }
    // save options
    function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['type'] = $new_instance['type'];
        $instance['class'] = $new_instance['class'];
        print_r($instance);
        return $instance;
    }
}
